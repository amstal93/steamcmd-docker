# Run this docker image with:
# docker run -e APP_ID="<app_id>" -e USERNAME="<username>" -e PASSWORD="<password>" -v ~/<game server folder>:/server steamcmd
# to login to steam anonymously remove the USERNAME and PASSWORD fields
FROM ubuntu:18.04

WORKDIR /steamcmd

RUN apt-get update -y
RUN apt-get install apt-utils -y
RUN apt-get install lib32gcc1 wget -y
RUN wget https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz
RUN tar -zxvf steamcmd_linux.tar.gz
RUN mkdir /server

CMD ./steamcmd.sh +login $USERNAME $PASSWORD +force_install_dir /server +app_update $APP_ID validate +quit
